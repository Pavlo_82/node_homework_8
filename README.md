## Завдання

Додати валідацію, обробку помилок та логування до новинного сервісу.

### Технічні вимоги

- Додати нові поля та змінити існуючі для моделі `newspostSchema`

   ```javascript
   const newspostSchema = {
     id: Number,
     title: String, // max 50 characters
     text: String, // max 256 characters
     genre: String, // support only 'Politic', 'Business', 'Sport', 'Other'
     isPrivate: Boolean, // ony true or false
     createDate: Date,
   }; +++++
   ```

- Ендпоінти `POST /api/newsposts/` та `PUT /api/newsposts/:id` мають валідувати вхідні данні, наприклад за допомогою бібліотеки `ajv` ++++++
- Додати створення та викидання кастомних помилок `ValidationError` при помилках валідації, та `NewspostsServiceError` при будь-якій іншій помилці у `NewspostsService`. Для демонстрації роботи `NewspostsServiceError` можна додати окремий ендпоінт `GET /api/newsposts/error`, який завжди буде викидати цю помилку
- Додати глобальний шар обробки помилок. Він має повертати статус 400, якщо виникає `ValidationError`, статус 500 при `NewspostsServiceError` чи у випадку будь-якої іншої помилки
- Додати систему логування. Для цього можна використати бібліотеку `winston`
- Усі логи необхідно писати в консоль, а також в лог файл. Застосунок має містити два лог файли - `application.log`, та `error.log`. `application.log` має включати усі створені логи. `error.log` має містити тільки логи рівня `ERROR`.
- Логувати усі вхідні запити:
  - рівень логування має бути `INFO`
  - запис має включати ім'я методу, URL адресу запиту та тіло запиту (якщо воно є)
- Логувати усі помилки, які трапляються в застосунку
  - якщо трапилась помилка `ValidationError`, логувати повідомлення помилки із рівнем логування `WARN`
  - якщо трапилась будь-яка інша помилка, логувати повідомленя помилки, а також повний стек трейс із рівнем логування `ERROR`
- Якщо у застосунку виникла помилка, повертати на фронтент відповідь в залежності від значення змінної оточення `NODE_ENV`. При значенні `development`, якщо при виконанні будь-якого запиту сталася помилка, користувачу? окрім повідомлення і коду, має повертатися увесь стек трейс помилки. При значенні `production` - у випадку виникнення помилки віддавати назад лише код і повідомлення.

### Необов'язкове завдання підвищеної складності

- Підключити до проекту `Sentry` та писати логи додатково ще й туди.
- Додати систему ротації лог файлів:
  - Загальні логи мають писатися в новий файл кожен день. Назва файлу має бути `application-yyyy-mm-dd.log`
  - Макимальний розмір файлу `error.log` має бути 1 мегабайт. При досягненні максимального розміру, помилки мають починати писатися в новий файл