import {
  ITable,
  INews,
  FieldSchema,
} from '../types/params.interface.ts';
import { nanoid } from 'nanoid';
import { existsSync } from 'fs';
import { promises as fs } from 'fs';
import * as path from 'path';
import url from 'url';

const __filename = url.fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

class Table implements ITable {
  private folder: string;
  private file: string;
  private schema: FieldSchema;
  private exists: boolean;
  constructor(database: string, schema: any) {
    this.schema = schema;
    if (!this.schema) {
      throw new Error('table not exist');
    }
    this.folder = path.join(__dirname, 'db');
    this.file = path.join(this.folder, `${database}.json`);
    this.exists = existsSync(this.folder);

    if (!this.exists) {
      this.initDB();
    }
  }
  private findEl (data: INews[], id: string): INews | undefined {
    return data.find(item => item.id === id);
  };

  private async initDB(): Promise<void> {
    try {
      await fs.mkdir(this.folder, { recursive: true });
      await fs.writeFile(this.file, JSON.stringify([]));
    } catch (err) {
      console.error('Error initializing database:', err);
    }
  }

  private async readData(): Promise<any> {
    const res: string = await fs.readFile(this.file, 'utf8');
    return JSON.parse(res);
  }

  private async writeData(data: any): Promise<void> {
    const jsonData: string = JSON.stringify(data);
    await fs.writeFile(this.file, jsonData);
  }

  // Example methods for working with news posts
  public async getAll(): Promise<INews[]> {
    return this.readData();
  }
  // <any>
  public async getById(id: string): Promise<any> {
    const data = await this.readData();
    const found = this.findEl(data, id);
    
    if (!found) {
      return { message: 'Not found' };
    }
    return found;
  }

  public async create(newspost: INews): Promise<INews> {
    const data = await this.readData();
    const id = nanoid();
    const createDate = new Date()
    const created = { ...newspost, id, createDate };
    data.push(created);
    await this.writeData(data);
    return await created;
  }

  public async update(id: string, updatedData: INews): Promise<any> {
    const data = await this.readData();
    const found = this.findEl(data, id);
    const findedId = data.findIndex((el: INews) => el.id === id);
    if (!found) {
      return { message: 'Not found' };
    }
    const updatedText = updatedData.text || found.text;
    const updatedTitle = updatedData.title || found.title;
    const updated = {
      id: found.id,
      text: updatedText,
      title: updatedTitle,
    };
    Object.assign(data[findedId], updated);
    await this.writeData(data);
    return updated;
  }

  public async delete(id: string): Promise<any> {
    const data = await this.readData();
    const index = data.findIndex((item: INews) => item.id === id);
    if (!index) {
      return { message: 'Not found' };
    }
    data.splice(index, 1);
    await this.writeData(data);
    return { message: 'delete successful' };
  }
}

export default Table;
