import {FileDB} from "./fileDb.ts";
const newsSchema = {
  id: String,
  title: String,
  text: String,
  createDate: Date,
  updateDate: Date,
};

const dbName = 'db';
export const createTable = async () => {
  const registerSchema = await FileDB.getInstance();
  await registerSchema.registerSchema(dbName, newsSchema);
  const Table = await registerSchema.getTable(dbName);
  return Table
};

