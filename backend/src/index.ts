import 'reflect-metadata';
import App from './server/app.ts';
import NewspostsController from './server/newsposts/newsposts.controller.ts';
import { Container } from 'typedi';
import config from './config/index.ts';
import { FileDB } from './fileDb/fileDb.ts';

const PORT = config.get('PORT');
const HOST = config.get('HOST');

const newsSchema = {
  id: String,
  title: String,
  text: String,
  createDate: Date,
  updateDate: Date,
};

export const createTable = async () => {
  const dbName = 'db';
  const registerSchema = await FileDB.getInstance();
  await registerSchema.registerSchema(dbName, newsSchema);
};

const init = async (PORT: number) => {
  await createTable()
  const app = await new App([Container.get(NewspostsController)], PORT);
  app.listen();
};

init(PORT);
