export interface Newspost {
  title: string;
  text: string;
  genre: 'Politic' | 'Business' | 'Sport' | 'Other';
  isPrivate: Boolean,
  createDate: Date,
}

export interface PagedNewsPosts {
  newsposts: Newspost[];
  total: number;
  size: number;
  page: number;
}
