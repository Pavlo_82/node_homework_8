import React from "react";

import CreateForm from "../../components/CreateForm";

const CreateNewsPostPage: React.FC = () => {
  return (
    <div className="max-w-[1200px] mx-auto my-[20px]">
      <h4>Create NewsPost</h4>
      <CreateForm/>
    </div>
  );
};

export default CreateNewsPostPage;
