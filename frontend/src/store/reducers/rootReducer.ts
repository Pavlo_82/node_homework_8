import { combineReducers } from "redux";
import NewspostsReducer from "./newspostsReducer";
import ModalReducer from "./modalReducer";

//combine reducers
const rootReducer = combineReducers({
    newsposts: NewspostsReducer,
    modal: ModalReducer
});

export default rootReducer;